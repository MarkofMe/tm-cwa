TeesMo - Customer Web App
=======================

Project Desciption

### Technologies

| Area                      |  Technologies                             |
|:--------------------------|:------------------------------------------|
| Hosting Platforms         | Azure                                         |
| Development Platforms     | Linux, Windows, OSX                       |
| Build Automation          | [FAKE](http://fsharp.github.io/FAKE/)     |
| Unit Testing              | [xUnit](https://github.com/xunit/xunit)            |
| Package Formats           | Nuget packages                            |
| Documentation Authoring   | Markdown, HTML and F# Literate Scripts    |

### Initializing

    Open Command Line and Navigate to the tm-cwa directory.
    Type " .\build.cmd" and press enter
    The project should build and install all of the nuget packages.

    If theres an error, try opening the solution and building from inside Visual Studio.
    Also try typing either;
        ".paket\paket.exe install" to attempt re-installing the packages.
        or
        ".paket\paket.exe update" to attempt to re-install and update the existing packages.

    If not building, contact me.

### Link

http://teesmo-customerwebapp.azurewebsites.net/webpages/Customer.html#!/search

