var Customer = angular.module('Customer', ['ngRoute']);

Customer.factory('LastPage', function () {
    var lastPage = {};
    return lastPage;
});

Customer.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/search', {
            templateUrl: 'Views/search.html',
            controller: 'searchController'
        })
        .when('/viewCar', {
            templateUrl: 'Views/carView.html',
            controller: 'carViewController'
        })
        .when('/manageTestDrive', {
            templateUrl: 'Views/testDriveManage.html',
            controller: 'manageTestDriveController'
        })
        .when('/bookTestDrive', {
            templateUrl: 'Views/testDriveBook.html',
            controller: 'bookTestDriveController'
        })
        .when('/manageServices', {
            templateUrl: 'Views/serviceManage.html',
            controller: 'manageServicesController'
        })
        .when('/bookService', {
            templateUrl: 'Views/serviceBook.html',
            controller: 'bookServicesController'
        })
        .when('/myAccount', {
            templateUrl: 'Views/myAccount.html',
            controller: 'myAccountController'
        })
        .when('/editAccount', {
            templateUrl: 'Views/myAccountEdit.html',
            controller: 'editAccountController'
        })
        .when('/login', {
            templateUrl: 'Views/login.html',
            controller: 'loginController'
        })
        .when('/createAccount', {
            templateUrl: 'Views/createAccount.html',
            controller: 'createAccountController'
        })
        .when('/tandC', {
            templateUrl: 'Views/tandC.html'
        })
        .when('/apologies', {
            templateUrl: 'Views/apologiesView.html'
        })
        .otherwise({
            redirectTo: '/search'
        });
}]);

Customer.controller('searchController', function ($scope, $http, LastPage) {
    
    var userData = JSON.parse(localStorage.getItem("userData"));

    if (userData === null) {
        $('#accountLogSelect').text("Log In");
    }
    else {
        $('#accountLogSelect').text("My Account");
    }

    var data = '{"Make" : "","Model" : "","YearUpperBound" : 9999,"YearLowerBound" : 9999,"Colour" : "","BodyType" : "","Doors" : 10,"GearBoxType" : "","MilesUpperBound" : 1000000,"MilesLowerBound" : 1000000,"EngineSizeUpperBound" : 99.9,"EngineSizeLowerBound" : 99.9,"SixtyTimeUpperBound" : 99.9,"SixtyTimeLowerBound" :  99.9,"FuelType" : "","DriveTrainType" : "","Co2UpperBound" : 9999,"Co2LowerBound" : 9999, "PriceUpperBound" : 999999999.9,"PriceLowerBound" : 999999999.9}';
    console.log(LastPage.lastPage);

        $.ajax({
            method: "POST",
            url: 'https://teesmo-carcomponent.azurewebsites.net/cc/Search',
            dataType: 'Json',
            data: data,
            success: function (results) {
                console.log(results);
                $('#searchPageResultsList')
                    .empty();

                $.each(results, function (index, t) {
                    var title = t.make + " " + t.model;

                    var data = t.year + " | " + t.bodyType + " | " + t.miles + "miles | " + t.gearBoxType + " | " + t.engineSize + "L | " + t.horsePower + "bhp | " + t.fuelType;

                    $('#searchPageResultsList').append('<li><article><div id="searchResultdiv"><div id="searchResultImagediv"><img id="imageOutput' + t.carID + '"></div><div id="searchResultDatadiv"><h2><a id="viewCar-' + t.carID + '" href="#!/viewCar#type=viewCar&carID=' + t.carID + '">' + title + '</a></h2><p>' + data + '</p><p>&pound' + t.price + '</p></div></div ></article></li>');

                    var output2 = document.getElementById('imageOutput' + t.carID);
                    output2.src = t.imageString;
                    output2.style.height = '200px';
                    output2.style.width = '270px';
                });

            }
        });

    $.ajax({
        url: 'https://teesmo-carcomponent.azurewebsites.net/cc/GetSearchParam',
        dataType: 'Json',
        success: function (results) {
            console.log(results)

            $.each(results.makes, function (index, t) {
                $('#carSearch-Make').append($("<option></option>")
                    .val(t)
                    .text(t));
            });

            $.each(results.years, function (index, t) {
                $('#carSearch-yearUpperBound').append($("<option></option>")
                    .val(t)
                    .text(t));
            });

            $.each(results.years, function (index, t) {
                $('#carSearch-yearLowerBound').append($("<option></option>")
                    .val(t)
                    .text(t));
            });

            $.each(results.colour, function (index, t) {
                $('#carSearch-Colour').append($("<option></option>")
                    .val(t)
                    .text(t));
            });
                
            $.each(results.miles, function (index, t) {
                $('#carSearch-milesUpperBound').append($("<option></option>")
                    .val(t)
                    .text(t));
            });

            $.each(results.miles, function (index, t) {
                $('#carSearch-milesLowerBound').append($("<option></option>")
                    .val(t)
                    .text(t));
            });

            $.each(results.engineSize, function (index, t) {
                $('#carSearch-engineSizeUpperBound').append($("<option></option>")
                    .val(t)
                    .text(t));
            });

            $.each(results.engineSize, function (index, t) {
                $('#carSearch-engineSizeLowerBound').append($("<option></option>")
                    .val(t)
                    .text(t));
            });

            $.each(results.co2, function (index, t) {
                $('#carSearch-co2UpperBound').append($("<option></option>")
                    .val(t)
                    .text(t));
            });

            $.each(results.co2, function (index, t) {
                $('#carSearch-co2LowerBound').append($("<option></option>")
                    .val(t)
                    .text(t));
            });

            $.each(results.price, function (index, t) {
                $('#carSearch-priceUpperBound').append($("<option></option>")
                    .val(t)
                    .text(t));
            });

            $.each(results.price, function (index, t) {
                $('#carSearch-priceLowerBound').append($("<option></option>")
                    .val(t)
                    .text(t));
            });
        }
    });


    $("#carSearch-Make")
        .change(function () {
            $("#carSearch-Make option:selected").each(function () {
                console.log($(this).text());
                var make = $(this).text();

                $.ajax({
                    url: 'https://teesmo-carcomponent.azurewebsites.net/cc/GetModelSearchParam',
                    dataType: 'Json',
                    data: { make },
                    success: function (results) {
                        $('#carSearch-Model')
                            .empty()
                            .append('<option value="Any">Any</option>');

                        $.each(results, function (index, t) {
                            $('#carSearch-Model').append($("<option></option>")
                                .val(t)
                                .text(t));
                        });

                    }
                });
                        

            });
        })
        .change();

    $('#carParameterSearch').click(function () {

        var make = "";
        switch ($('#carSearch-Make').val()) {
            case "Any":
                make = ""
                break;
            default :
                make = $('#carSearch-Make').val();
                break;
        }

        var model = "";
        switch ($('#carSearch-Model').val()) {
            case "Any":
                model = ""
                break;
            default:
                model = $('#carSearch-Model').val();
                break;
        }

        var yearUpper = "";
        switch ($('#carSearch-yearUpperBound').val()) {
            case "Any":
                yearUpper = "9999";
                break;
            default:
                yearUpper = $('#carSearch-yearUpperBound').val();
                break;
        }

        var yearLower = "";
        switch ($('#carSearch-yearLowerBound').val()) {
            case "Any":
                yearLower = "9999";
                break;
            default:
                yearLower = $('#carSearch-yearLowerBound').val();
                break;
        }

        var colour = "";
        switch ($('#carSearch-Colour').val()) {
            case "Any":
                colour = "";
                break;
            default:
                colour = $('#carSearch-Colour').val();
                break;
        }

        var bodyType = "";
        switch ($('#carSearch-BodyType').val()) {
            case "Any":
                bodyType = "";
                break;
            default:
                bodyType = $('#carSearch-BodyType').val();
                break;
        }

        var doors = "";
        switch ($('#carSearch-Doors').val()) {
            case "Any":
                doors = "10";
                break;
            default:
                doors = $('#carSearch-Doors').val();
                break;
        }

        var gearBox = "";
        switch ($('#carSearch-GearBox').val()) {
            case "Any":
                gearBox = ""
                break;
            default:
                gearBox = $('#carSearch-GearBox').val();
                break;
        }

        var milesUpper = "";
        switch ($('#carSearch-milesUpperBound').val()) {
            case "Any":
                milesUpper = "1000000";
                break;
            default:
                milesUpper = $('#carSearch-milesUpperBound').val();
                break;
        }

        var milesLower = "";
        switch ($('#carSearch-milesLowerBound').val()) {
            case "Any":
                milesLower = "1000000";
                break;
            default:
                milesLower = $('#carSearch-milesLowerBound').val();
                break;
        }

        var engineSizeUpper = "";
        switch ($('#carSearch-engineSizeUpperBound').val()) {
            case "Any":
                engineSizeUpper = "99.9";
                break;
            default:
                engineSizeUpper = $('#carSearch-engineSizeUpperBound').val();
                break;
        }

        var engineSizeLower = "";
        switch ($('#carSearch-engineSizeLowerBound').val()) {
            case "Any":
                engineSizeLower = "99.9";
                break;
            default:
                engineSizeLower = $('#carSearch-engineSizeLowerBound').val();
                break;
        }

        var sixtyUpper = "";
        var sixtyLower = "";
        switch ($('#carSearch-0-60').val()) {
            case "Any":
                var sixtyUpper = "99.9";
                var sixtyLower = "99.9";
                break;
            case "0-5":
                var sixtyUpper = "5";
                var sixtyLower = "0";
                break;
            case "0-8":
                var sixtyUpper = "8";
                var sixtyLower = "0";
                break;
            case "0-10":
                var sixtyUpper = "10";
                var sixtyLower = "0";
                break;
            case "0-15":
                var sixtyUpper = "15";
                var sixtyLower = "0";
                break;

        }

        var fuelType = "";
        switch ($('#carSearch-FuelType').val()) {
            case "Any":
                fuelType = ""
                break;
            default:
                fuelType = $('#carSearch-FuelType').val();
                break;
        }

        var driveTrainType = "";
        switch ($('#carSearch-DriveTrain').val()) {
            case "Any":
                driveTrainType = ""
                break;
            default:
                driveTrainType = $('#carSearch-DriveTrain').val();
                break;
        }

        var co2UpperBound = "";
        switch ($('#carSearch-co2UpperBound').val()) {
            case "Any":
                co2UpperBound = "9999";
                break;
            default:
                co2UpperBound = $('#carSearch-co2UpperBound').val();
                break;
        }

        var co2LowerBound = "";
        switch ($('#carSearch-co2LowerBound').val()) {
            case "Any":
                co2LowerBound = "9999";
                break;
            default:
                co2LowerBound = $('#carSearch-co2LowerBound').val();
                break;
        }

        var priceUpperBound = "";
        switch ($('#carSearch-priceUpperBound').val()) {
            case "Any":
                priceUpperBound = "999999999.9";
                break;
            default:
                priceUpperBound = $('#carSearch-priceUpperBound').val();
                break;
        }

        var priceLowerBound = "";
        switch ($('#carSearch-priceLowerBound').val()) {
            case "Any":
                priceLowerBound = "999999999.9";
                break;
            default:
                priceLowerBound = $('#carSearch-priceLowerBound').val();
                break;
        }
        
        var data = '{"Make" : "' + make +
            '","Model" : "' + model +
            '","YearUpperBound" : ' + parseInt(yearUpper) +
            ',"YearLowerBound" : ' + parseInt(yearLower) +
            ',"Colour" : "' + colour +
            '","BodyType" : "' + bodyType +
            '","Doors" : ' + parseInt(doors) +
            ',"GearBoxType" : "' + gearBox +
            '","MilesUpperBound" : ' + parseInt(milesUpper) +
            ',"MilesLowerBound" : ' + parseInt(milesLower) +
            ',"EngineSizeUpperBound" : ' + parseFloat(engineSizeUpper) +
            ',"EngineSizeLowerBound" : ' + parseFloat(engineSizeLower) +
            ',"SixtyTimeUpperBound" : ' + parseFloat(sixtyUpper) +
            ',"SixtyTimeLowerBound" :  ' + parseFloat(sixtyLower) +
            ', "FuelType" : "' + fuelType +
            '", "DriveTrainType" : "' + driveTrainType +
            '", "Co2UpperBound" : ' + parseInt(co2UpperBound) +
            ', "Co2LowerBound" : ' + parseInt(co2LowerBound) +
            ', "PriceUpperBound" : ' + parseFloat(priceUpperBound) +
            ', "PriceLowerBound" : ' + parseFloat(priceLowerBound) +
        '}';

        $.ajax({
            method: "POST",
            url: 'https://teesmo-carcomponent.azurewebsites.net/cc/Search',
            dataType: 'Json',
            data: data,
            success: function (results) {
                console.log(results);
                $('#searchPageResultsList')
                    .empty();

                $.each(results, function (index, t) {
                    var title = t.make + " " + t.model;

                    var data = t.year + " | " + t.bodyType + " | " + t.miles + "miles | " + t.gearBoxType + " | " + t.engineSize + "L | " + t.horsePower + "bhp | " + t.fuelType;

                    $('#searchPageResultsList').append('<li><article><div id="searchResultdiv"><div id="searchResultImagediv"><img id="imageOutput' + t.carID + '"></div><div id="searchResultDatadiv"><h2><a id="viewCar-' + t.carID + '" href="#!/viewCar#type=viewCar&carID=' + t.carID + '">' + title + '</a></h2><p>' + data + '</p><p>&pound'+t.price+'</p></div></div ></article></li>');

                    var output2 = document.getElementById('imageOutput' + t.carID);
                    output2.src = t.imageString;
                    output2.style.height = '200px';
                    output2.style.width = '270px';
                });
            }
        });
    });
});

Customer.controller('carViewController', function ($scope, $http) {
    var location_vars = [];
    var location_vars_temp = location.hash.replace('#', ''); // Remove the hash sign
    location_vars_temp = location_vars_temp.split('&'); // Break down to key-value pairs
    for (i = 0; i < location_vars_temp.length; i++) {
        location_var_key_val = location_vars_temp[i].split('='); // Break down each pair
        location_vars.push(location_var_key_val[1]);
    }

    var userData = JSON.parse(localStorage.getItem("userData"));
    if (userData === null) {
        $('#accountLogSelect').text("Log In");
    }
    else {
    }

    console.log(location_vars);

    $.ajax({
        method: "POST",
        url: 'https://teesmo-carcomponent.azurewebsites.net/cc/CarDetails',
        dataType: 'Json',
        data: '{"carID" : ' + location_vars[1] + '}',
        success: function (results) {
            console.log(results);

            $('#viewCar-title').text(results[0].make + " " + results[0].model);
            var book = '<a href="#!/bookTestDrive#type=carView&carID=' + results[0].carID + '" > Book Test Drive</a >'
            if (location_vars[0] === "viewYourCar")
            {
                book = ''
            }


            $('#viewCarContainerDiv').empty();
            $('#viewCarContainerDiv').append('<div style="height:410px"><div class="viewCarImageDiv" id="viewCarImageDiv-' + results[0].carID + '"><img class="view-imageOutput" id="view-imageOutput' + results[0].carID + '"></div>\
                <div class="testDriveDiv" id= "testDriveDiv' + results[0].carID + '" >\
                <h3>Car Location:</h3>\
                <p>Dealership: '+ results[0].name +'</p>\
                <p>Address: '+ results[0].address +'</p>\
                <p>Town/City: '+ results[0].city + '</p>\
                <p>Postcode: '+ results[0].postCode +'</p>\
                <p>Number: '+ results[0].number +'</p>\
                <br/>'+ book+'\
                </div ></div > <br />\
                <div class="viewCarDataDiv" id="viewCarDataDiv' + results[0].carID + '"><ul>'+
                '<li class="carView_list" id="carView_list_year">' + results[0].year+'</li >'+
                '<li class="carView_list" id="carView_list_bodyType">' + results[0].bodyType+'</li>'+
                '<li class="carView_list" id="carView_list_miles">' + results[0].miles+'miles</li>'+
                '<li class="carView_list" id="carView_list_engineSize">' + results[0].engineSize + 'L</li>'+
                '<li class="carView_list" id="carView_list_gearbox">' + results[0].gearBoxType+'</li>'+
                '<li class="carView_list" id="carView_list_fuel">' + results[0].fuelType+'</li>'+
                                                                                        '</ul >'+
                                                                                        '<ul>' +
                '<li class="carView_list" id="carView_list_colour">' + results[0].colour+'</li>' +
                '<li class="carView_list" id="carView_list_doors">' + results[0].doors+' doors</li>' +
                '<li class="carView_list" id="carView_list_sixtyTime">' + results[0].sixtyTime+'s</li>' +
                '<li class="carView_list" id="carView_list_horsePower">' + results[0].horsePower+'bhp</li>' +
                '<li class="carView_list" id="carView_list_driveTrain">' + results[0].driveTrainType+'</li>' +
                '<li class="carView_list" id="carView_list_co2">' + results[0].co2 +'g/km</li>' +
                                                                                        '</ul></div>');

            var output2 = document.getElementById('view-imageOutput' + results[0].carID);
            output2.src = results[0].imageString;
            output2.style.height = '400px';
            output2.style.width = '550px';
        }
    });
});

Customer.controller('manageTestDriveController', function ($scope, $http, LastPage) {
    var lastPage = "manageTestDrive";
    LastPage.lastPage = lastPage;
    var userData = JSON.parse(localStorage.getItem("userData"));
    if (userData === null) {
        $('#accountLogSelect').text("Log In");
        window.location.href = '#!/login';
    }
    else {
        ManageTestDrives(userData);
    }
});

function ManageTestDrives(userData) {
    var customerID = userData[0].customerID;
    $.ajax({
        method: "POST",
        url: 'https://teesmo-bookingcomponent.azurewebsites.net/TDB/ByCustomerId',
        dataType: 'Json',
        data: '{"CustomerID" : ' + customerID + '}',
        success: function (results) {
            console.log(results);
            $('#manageTestDrivesTable').empty();
            $('#manageTestDrivesTable').append('<tr><th>Test Drive ID</th><th>Car</th><th>Accompanying Staff Member</th><th>Date</th><th>Edit</th><th>Cancel</th></tr>');


            $.each(results, function (index, t) {
                $('#manageTestDrivesTable').append('<tr><td>' + t.testDriveBookingID + '</td><td>' + t.car + '</td><td>' + t.staffName + '</td><td>' + t.dateBooked + '</td><td><a href="#!/bookTestDrive#type=editExisting&testdriveID=' + t.testDriveBookingID + '">Edit Test Drive</a></td><td><a id="cancelTestDriveBooking-' + t.testDriveBookingID + '">Cancel Booking</a></td></tr>');

                $('#cancelTestDriveBooking-' + t.testDriveBookingID).click(function () {
                    var accept = confirm("Do you want to continue?");
                    if (accept) {
                        $.ajax({
                            method: "POST",
                            url: 'https://teesmo-bookingcomponent.azurewebsites.net/TDB/UpdateTestDriveBooking',
                            dataType: 'Json',
                            data: '{"TestDriveBookingID" : ' + t.testDriveBookingID + ',"CustomerID": ' + t.customerID + ',"CarID": ' + t.carID + ',"StaffID": ' + t.staffID + ',"Name": "' + t.name + '","DateBooked": "' + t.dateBooked + '","Active": false}',
                            success: function (results) { ManageTestDrives(userData); }
                        });
                    }
                    
                });
            });
        }
    });
}

Customer.controller('bookTestDriveController', function ($scope, $http, LastPage) {
    var lastPage = "bookTestDrive";
    var existData = "";
    LastPage.lastPage = lastPage;
    var userData = JSON.parse(localStorage.getItem("userData"));
    if (userData === null) {
        $('#accountLogSelect').text("Log In");
        window.location.href = '#!/login';
    }
    else {

        $('#carSearchSelect').removeClass('selected').addClass('notselected');
        $('#testDriveSelect').removeClass('notselected').addClass('selected');
        $('#serviceSelect').removeClass('selected').addClass('notselected');
        $('#accountLogSelect').removeClass('selected').addClass('notselected');

        var date = new Date();
        var days = new Date(2018, (date.getMonth() + 1), 0).getDate();

        console.log(days);

        $('#bookTestDrive-Month').empty();
        for (var i = (date.getMonth() + 1); i < 13; i++) {
            if (i.toString().length === 1) {
                $('#bookTestDrive-Month').append('<option value="0' + i + '">0' + i + '</option>');
            }
            else {
                $('#bookTestDrive-Month').append('<option value="' + i + '">' + i + '</option>');
            }
            
        }

        $('#bookTestDrive-Day').empty();
        for (var i = (date.getDate() + 1); i < days + 1; i++) {
            if (i.toString().length === 1) {
                $('#bookTestDrive-Day').append('<option value="0' + i + '">0' + i + '</option>');
            }
            else {
                $('#bookTestDrive-Day').append('<option value="' + i + '">' + i + '</option>');
            }

        }


        var data = '{"Make" : "","Model" : "","YearUpperBound" : 9999,"YearLowerBound" : 9999,"Colour" : "","BodyType" : "","Doors" : 10,"GearBoxType" : "","MilesUpperBound" : 1000000,"MilesLowerBound" : 1000000,"EngineSizeUpperBound" : 99.9,"EngineSizeLowerBound" : 99.9,"SixtyTimeUpperBound" : 99.9,"SixtyTimeLowerBound" :  99.9,"FuelType" : "","DriveTrainType" : "","Co2UpperBound" : 9999,"Co2LowerBound" : 9999, "PriceUpperBound" : 999999999.9,"PriceLowerBound" : 999999999.9}';
        
        var ajax = $.ajax({
                        method: "POST",
                        url: 'https://teesmo-carcomponent.azurewebsites.net/cc/Search',
                        dataType: 'Json',
                        data: data,
                        success: function (results) {
                            $.each(results, function (index, t) {
                                $('#bookTestDrive-Car').append('<option value='+t.carID+'>'+t.make+' '+t.model+' '+t.year+'</option>');
                            });
                        }
                    });


        var location_vars = [];
        var location_vars_temp = location.hash.replace('#', ''); // Remove the hash sign
        location_vars_temp = location_vars_temp.split('&'); // Break down to key-value pairs
        for (i = 0; i < location_vars_temp.length; i++) {
            location_var_key_val = location_vars_temp[i].split('='); // Break down each pair
            location_vars.push(location_var_key_val[1]);
        }

        if (location_vars[0] == undefined) {
            console.log("No Car ID");
        }
        else if (location_vars[0] == "carView") {
            console.log("carView");
            $.when(ajax).done(function () {
                $('#bookTestDrive-Car').val(location_vars[1]);
            });
        }
        else if(location_vars[0] == "editExisting") {
            console.log("editExisting");
            $.when(ajax).done(function () {
                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-bookingcomponent.azurewebsites.net/TDB/ById',
                    dataType: 'Json',
                    data: '{"testDriveBookingID" : ' + location_vars[1] + '}',
                    success: function (results) {
                        existData = results[0];
                        console.log(results[0]);
                        $('#bookTestDrive-Car').val(results[0].carID);
                        var dateBooked = (results[0].dateBooked).split("-");
                        console.log(dateBooked);
                        var day = dateBooked[2].substring(0, 2);
                        var time = dateBooked[2].split("T");
                        var timeCompo = time[1].split(":");
                        var month = dateBooked[1];
                        var hour = timeCompo[0];
                        var minute = timeCompo[1];

                        console.log(day);
                        console.log(month);
                        console.log(hour);
                        console.log(minute);

                        $('#bookTestDrive-Day').val(day);
                        $('#bookTestDrive-Month').val(month);
                        $('#bookTestDrive-Hour').val(hour);
                        $('#bookTestDrive-Minute').val(minute);
                    }
                });
            });
        }

        $('#bookTestDrive-button').click(function () {
            if (location_vars[0] == "editExisting") {
                console.log("editExisting");
                var customerID = userData[0].customerID;
                var carID = $('#bookTestDrive-Car').val();
                var dateBooked = $('#bookTestDrive-Year').val() + "-" + $('#bookTestDrive-Month').val() + "-" + $('#bookTestDrive-Day').val() + ' ' + $('#bookTestDrive-Hour').val() + ":" + $('#bookTestDrive-Minute').val();
                console.log(existData);
                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-bookingcomponent.azurewebsites.net/TDB/UpdateTestDriveBooking',
                    dataType: 'Json',
                    data: '{"TestDriveBookingID" : ' + existData.testDriveBookingID + ',"CustomerID": ' + customerID + ',"CarID": ' + carID + ',"StaffID": ' + existData.staffID + ',"Name": "' + existData.name + '","DateBooked": "' + dateBooked + '","Active": true}',
                    success: function (results) { window.location.href = '#!/manageTestDrive';}
                });
            } else {

                var customerID = userData[0].customerID;
                var carID = $('#bookTestDrive-Car').val();
                var dateBooked = $('#bookTestDrive-Year').val() + "-" + $('#bookTestDrive-Month').val() + "-" + $('#bookTestDrive-Day').val() + ' ' + $('#bookTestDrive-Hour').val() + ":" + $('#bookTestDrive-Minute').val();

                console.log(customerID);
                console.log(carID);
                console.log(dateBooked);

                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-bookingcomponent.azurewebsites.net/TDB/InsertTestDriveBooking',
                    dataType: 'Json',
                    data: '{"CustomerID" : ' + parseInt(customerID) + ' , "CarID" : ' + parseInt(carID) + ' , "DateBooked" : "' + dateBooked.toString() + '"}',
                    success: function (results) { window.location.href = '#!/manageTestDrive';}
                });
            }

        });
    }
});

Customer.controller('manageServicesController', function ($scope, $http, LastPage) {
    var lastPage = "manageServices";
    LastPage.lastPage = lastPage;
    var userData = JSON.parse(localStorage.getItem("userData"));
    if (userData === null) {
        $('#accountLogSelect').text("Log In");
        window.location.href = '#!/login';
    }
    else {
        ManageServices(userData);
    }
});

function ManageServices(userData) {
    var customerID = userData[0].customerID;
    $.ajax({
        method: "POST",
        url: 'https://teesmo-bookingcomponent.azurewebsites.net/SB/ByCustomerId',
        dataType: 'Json',
        data: '{"CustomerID" : ' + customerID + '}',
        success: function (results) {
            console.log(results);
            $('#manageServicesTable').empty();
            $('#manageServicesTable').append('<tr><th>Service ID</th><th>Car</th><th>Dealership</th><th>Date</th><th>Edit</th><th>Cancel</th></tr>');


            $.each(results, function (index, t) {
                $('#manageServicesTable').append('<tr><td>' + t.serviceBookingID + '</td><td>' + t.car + '</td><td>' + t.dealershipName + '</td><td>' + t.dateBooked + '</td><td><a href="#!/bookService#type=editExisting&serviceID=' + t.serviceBookingID + '">Edit Service</a></td><td><a id="cancelServiceBooking-' + t.serviceBookingID + '">Cancel Booking</a></td></tr>');

                $('#cancelServiceBooking-' + t.serviceBookingID).click(function () {
                    var accept = confirm("Do you want to continue?");
                    if (accept) {
                        $.ajax({
                            method: "POST",
                            url: 'https://teesmo-bookingcomponent.azurewebsites.net/SB/UpdateServiceBooking',
                            dataType: 'Json',
                            data: '{"ServiceBookingID" : ' + t.serviceBookingID + ',"CustomerID": ' + t.customerID + ',"CarID": ' + t.carID + ',"StaffID": ' + t.staffID + ',"DateBooked": "' + t.dateBooked + '", "DealershipName" : "' + t.dealershipName + '","Active": false}',
                            success: function (results) { ManageServices(userData); }
                        });
                    }
                    
                });

            });
        }
    });
}

Customer.controller('bookServicesController', function ($scope, $http, LastPage) {
    var lastPage = "bookServices";
    var existData = "";
    LastPage.lastPage = lastPage;
    var userData = JSON.parse(localStorage.getItem("userData"));
    if (userData === null) {
        $('#accountLogSelect').text("Log In");
        window.location.href = '#!/login';
    }
    else {

        var date = new Date();
        var days = new Date(2018, (date.getMonth() + 1), 0).getDate();

        console.log(days);

        $('#bookService-Month').empty();
        for (var i = (date.getMonth() + 1); i < 13; i++) {
            if (i.toString().length === 1) {
                $('#bookService-Month').append('<option value="0' + i + '">0' + i + '</option>');
            }
            else {
                $('#bookService-Month').append('<option value="' + i + '">' + i + '</option>');
            }

        }

        $('#bookService-Day').empty();
        for (var i = (date.getDate() + 1); i < days + 1; i++) {
            if (i.toString().length === 1) {
                $('#bookService-Day').append('<option value="0' + i + '">0' + i + '</option>');
            }
            else {
                $('#bookService-Day').append('<option value="' + i + '">' + i + '</option>');
            }

        }

        var ajax = $.ajax({
            method: "POST",
            url: 'https://teesmo-carcomponent.azurewebsites.net/cc/ByCustomerID',
            dataType: 'Json',
            data: '{ "customerID": ' + userData[0].customerID + ' }',
            success: function (results) {
                console.log(results);
                $.each(results, function (index, t) {
                    $('#bookService-Car').append('<option value=' + t.carID + '>' + t.car + '</option>');
                });
            }
        });

        var location_vars = [];
        var location_vars_temp = location.hash.replace('#', ''); // Remove the hash sign
        location_vars_temp = location_vars_temp.split('&'); // Break down to key-value pairs
        for (i = 0; i < location_vars_temp.length; i++) {
            location_var_key_val = location_vars_temp[i].split('='); // Break down each pair
            location_vars.push(location_var_key_val[1]);
        }

        if (location_vars[0] == undefined) {
            console.log("No Car ID");
        }
        else if (location_vars[0] == "editExisting") {
            console.log("editExisting");
            $.when(ajax).done(function () {
                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-bookingcomponent.azurewebsites.net/SB/ById',
                    dataType: 'Json',
                    data: '{"ServiceBookingID" : ' + location_vars[1] + '}',
                    success: function (results) {
                        existData = results[0];
                        console.log(results[0]);

                        $('#bookService-Dealership').val(results[0].dealershipName);

                        $('#bookService-Car').val(results[0].carID);
                        var dateBooked = (results[0].dateBooked).split("-");
                        console.log(dateBooked);
                        var day = dateBooked[2].substring(0, 2);
                        var time = dateBooked[2].split("T");
                        var timeCompo = time[1].split(":");
                        var month = dateBooked[1];
                        var hour = timeCompo[0];
                        var minute = timeCompo[1];

                        console.log(day);
                        console.log(month);
                        console.log(hour);
                        console.log(minute);

                        $('#bookService-Day').val(day);
                        $('#bookService-Month').val(month);
                        $('#bookService-Hour').val(hour);
                        $('#bookService-Minute').val(minute);
                    }
                });
            });
        }

        $('#bookService-button').click(function () {
            if (location_vars[0] == "editExisting") {
                console.log("editExisting");
                var customerID = userData[0].customerID;
                var carID = $('#bookService-Car').val();
                var dateBooked = $('#bookService-Year').val() + "-" + $('#bookService-Month').val() + "-" + $('#bookService-Day').val() + ' ' + $('#bookService-Hour').val() + ":" + $('#bookService-Minute').val();
                console.log(existData);
                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-bookingcomponent.azurewebsites.net/SB/UpdateServiceBooking',
                    dataType: 'Json',
                    data: '{"ServiceBookingID" : ' + existData.serviceBookingID + ',"CustomerID": ' + parseInt(customerID) + ',"CarID": ' + parseInt(carID) + ',"StaffID": ' + parseInt(existData.staffID) + ',"DateBooked": "' + dateBooked + '", "DealershipName" : "' + $('#bookService-Dealership').val() + '","Active": true}',
                    success: function (results) { window.location.href = '#!/manageServices'; }
                });
            } else {

                var customerID = userData[0].customerID;
                var carID = $('#bookService-Car').val();
                var dateBooked = $('#bookService-Year').val() + "-" + $('#bookService-Month').val() + "-" + $('#bookService-Day').val() + ' ' + $('#bookService-Hour').val() + ":" + $('#bookService-Minute').val();

                console.log(customerID);
                console.log(carID);
                console.log(dateBooked);

                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-bookingcomponent.azurewebsites.net/SB/InsertNewServiceBooking',
                    dataType: 'Json',
                    data: '{"CustomerID" : ' + parseInt(customerID) + ' , "CarID" : ' + parseInt(carID) + ' , "DateBooked" : "' + dateBooked.toString() + '", "DealershipName" : "' + $('#bookService-Dealership').val() + '"}',
                    success: function (results) { window.location.href = '#!/manageServices'; }
                });
            }

        });
    }
});

Customer.controller('loginController', function ($scope, $http, LastPage) {
    console.log(LastPage.lastPage);
    $("#logres").text('Enter UserName and Password');

    $("#login-form").validate(); //Jquery validate

    $("#usernameinput").rules('add', {
        required: true
    });

    $("#passwordinput").rules('add', {
        required: true
    });

    $("#login-form").validate();

    $('#loginButton').click(function () {

        var uName = $("#usernameinput").val();
        var pass = $("#passwordinput").val();

        $.ajax({
            method: "POST",
            url: 'https://teesmo-profilecomponent.azurewebsites.net/CP/login',
            dataType: 'Json',
            data: '{"CustomerUserName": "' + uName + '","CustomerPassword": "' + pass+'"}',
            success: function (userData) {
                console.log(userData);
                if (userData[0].status === "OK") {
                    $("#logres").text("");
                    $("#logres").text('UserName and Password good');
                    localStorage.setItem("userData", JSON.stringify(userData));
                    localStorage.setItem("password", pass);
                    //UserData.userData = userData;
                    //Password.password = pass;
                    console.log(JSON.parse(localStorage.getItem("userData")));
                    console.log(LastPage.lastPage);

                    $('#accountLogSelect').text("My Account");

                    if (LastPage.lastPage === "manageTestDrive") {
                        window.location.href = '#!/manageTestDrive';
                    } else if (LastPage.lastPage === "bookTestDrive") {
                        window.location.href = '#!/bookTestDrive';
                    } else if (LastPage.lastPage === "manageServices") {
                        window.location.href = '#!/manageServices';
                    } else if (LastPage.lastPage === "bookService") {
                        window.location.href = '#!/bookService';
                    } else if (LastPage.lastPage === "myAccount") {
                        window.location.href = '#!/myAccount';
                    }
                }
                else if (userData[0].status === "PASSWORD") {
                    $("#logres").text("");
                    $("#logres").text('Incorrect Password');

                }
                else if (userData[0].status === "INACTIVE") {
                    $("#logres").text("");
                    $("#logres").text('Account is Inactive');

                }
                else if (userData[0].status === "WRONG") {
                    $("#logres").text("");
                    $("#logres").text('Account does not exist');

                }
            }
        });
    });
});

Customer.controller('myAccountController', function ($scope, $http, LastPage) {
    var lastPage = "myAccount";
    console.log(LastPage.lastPage);
    LastPage.lastPage = lastPage;
    var userData = JSON.parse(localStorage.getItem("userData"));
    if (userData === null) {
        $('#accountLogSelect').text("Log In");
        window.location.href = '#!/login';
    }
    else {
        $('#customerAccount-userName').val(userData[0].customerUserName);
        $('#customerAccount-firstName').val(userData[0].firstName);
        $('#customerAccount-lastName').val(userData[0].lastName);
        $('#customerAccount-email').val(userData[0].email);
        $('#customerAccount-number').val(userData[0].houseNumber);
        $('#customerAccount-address1').val(userData[0].address1);
        $('#customerAccount-address2').val(userData[0].address2);
        $('#customerAccount-city').val(userData[0].town);
        $('#customerAccount-postcode').val(userData[0].postcode);
        $('#customerAccount-yourCars').empty();
        var ajax = $.ajax({
            method: "POST",
            url: 'https://teesmo-carcomponent.azurewebsites.net/cc/ByCustomerID',
            dataType: 'Json',
            data: '{ "customerID": ' + userData[0].customerID + ' }',
            success: function (results) {
                console.log(results);
                
                $('#customerAccount-yourCars').append('<tr><th>Car</th><th>View Car</th></tr>');
                $.each(results, function (index, t) {
                    $('#customerAccount-yourCars').append('<tr><td>' + t.car + '</td><td><a id="viewCar-' + t.carID + '" href="#!/viewCar#type=viewYourCar&carID=' + t.carID + '">View Car</a></td></tr>');
                });
            }
        });
    }

    $('#customerAccount-deleteAccount').click(function () {
        $.ajax({
            method: "POST",
            url: 'https://teesmo-profilecomponent.azurewebsites.net/CP/UpdateCustomer',
            dataType: 'Json',
            data: '{"CustomerID": ' + userData[0].customerID + ',"FirstName": "' + userData[0].firstName + '","LastName": "' + userData[0].lastName + '","Email": "' + userData[0].email +
            '","HouseNumber": "' + userData[0].houseNumber + '","Address1": "' + userData[0].address1 + '","Address2": "' + userData[0].address2 +
            '","Town": "' + userData[0].town + '","Postcode": "' + userData[0].postcode + '","Active": false}',
            success: function () {
                window.location.href = '#!/login';
            }
        });
    });
});

Customer.controller('editAccountController', function ($scope, $http) {
    var userData = JSON.parse(localStorage.getItem("userData"));
    if (userData === null) {
        $('#accountLogSelect').text("Log In");
        window.location.href = '#!/login';
    }
    else {
        console.log(userData[0]);

        $("#editAccountDiv").validate({
            rules: {
                firstName: {
                    required: true
                },
                lastName: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                number: {
                    required: true
                },
                address1: {
                    required: true
                },
                address2: {
                    required: false
                },
                city: {
                    required: true
                },
                postcode: {
                    required: true
                }
            },
            messages: {
                firstName: "Please enter your firstname",
                lastName: "Please enter your lastname",
                email: "Please enter a valid email address",
                number: {
                    required: "Please enter your phone number",
                    number: "Please enter your phone number",
                    minlength: "Please enter your phone number",
                },
                address1: "Please enter the first line of your address, house number included.",
                address2: "Please enter the second line of your address, if you have one.",
                city: "Please enter the name of your town/city",
                postcode: "Please enter your postcode"
            }
        });
        
        $('#editAccount-firstName').val(userData[0].firstName);
        $('#editAccount-lastName').val(userData[0].lastName);
        $('#editAccount-email').val(userData[0].email);
        $('#editAccount-number').val(userData[0].houseNumber);
        $('#editAccount-address1').val(userData[0].address1);
        $('#editAccount-address2').val(userData[0].address2);
        $('#editAccount-city').val(userData[0].town);
        $('#editAccount-postcode').val(userData[0].postcode);

        //$("#editAccountDiv").validate();

        $('#editAccount-Button').click(function () {
            if ($("#editAccountDiv").valid() === false) {
                alert("Invalid Input");
            }
            else
            {
                var firstName = $('#editAccount-firstName').val();
                var LastName = $('#editAccount-lastName').val();
                var email = $('#editAccount-email').val();
                var houseNumber = $('#editAccount-number').val();
                var address1 = $('#editAccount-address1').val();
                var address2 = $('#editAccount-address2').val();
                var city = $('#editAccount-city').val();
                var postCode = $('#editAccount-postcode').val().toUpperCase();

                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-profilecomponent.azurewebsites.net/CP/UpdateCustomer',
                    dataType: 'Json',
                    data: '{"CustomerID": ' + userData[0].customerID + ',"FirstName": "' + firstName + '","LastName": "' + LastName + '","Email": "' + email +
                    '","HouseNumber": "' + houseNumber + '","Address1": "' + address1 + '","Address2": "' + address2 +
                    '","Town": "' + city + '","Postcode": "' + postCode + '","Active": true}',
                    success: function (results) {

                        $.ajax({
                            method: "POST",
                            url: 'https://teesmo-profilecomponent.azurewebsites.net/CP/login',
                            dataType: 'Json',
                            data: '{"CustomerUserName": "' + userData[0].customerUserName + '","CustomerPassword": "' + localStorage.getItem("password") + '"}',
                            success: function (userData) {
                                localStorage.setItem("userData", JSON.stringify(userData));
                                window.location.href = '#!/myAccount';
                            }
                        });
                    }
                });
            }
        });
    }
});

Customer.controller('createAccountController', function ($scope, $http) {

    var userData = JSON.parse(localStorage.getItem("userData"));
    if (userData === null) {
        $('#accountLogSelect').text("Log In");
    }
    else {
    }

    $("#createAccountDiv").validate({
        rules: {
            userName: {
                required: true,
                minlength: 8
            },
            password: {
                minlength: 8,
                required: true
            },
            firstName: {
                required: true
            },
            lastName: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            number: {
                number: true,
                minlength: 8,
                required: true
            },
            address1: {
                required: true
            },
            address2: {
                required: false
            },
            city: {
                required: true
            },
            postcode: {
                required: true
            }
        },
        messages: {
            userName: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 8 characters"
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 8 characters long"
            },
            firstName: "Please enter your firstname",
            lastName: "Please enter your lastname",
            email: "Please enter a valid email address",
            number: {
                required: "Please enter your phone number",
                number: "Please enter your phone number",
                minlength: "Please enter your phone number",
            },
            address1: "Please enter the first line of your address, house number included.",
            address2: "Please enter the second line of your address, if you have one.",
            city: "Please enter the name of your town/city",
            postcode: "Please enter your postcode"
        }
    });

    $('#newCustomerCreateButton').click(function () {
        if ($("#createAccountDiv").valid() === false) {
            alert("Invalid Input");
        }
        else {
            if ($("#newCustomer-permission").is(':checked')) {
                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-profilecomponent.azurewebsites.net/CP/InsertNewCustomer',
                    dataType: 'Json',
                    data: '{"CustomerUserName": "' + $('#newCustomer-userName').val() + '","CustomerPassword": "' + $('#newCustomer-password').val() + '","FirstName": "' + $('#newCustomer-firstName').val() +
                        '","LastName": "' + $('#newCustomer-lastName').val() + '","Email": "' + $('#newCustomer-email').val() + '","HouseNumber": "' + $('#newCustomer-number').val() +
                        '","Address1": "' + $('#newCustomer-address1').val() + '","Address2": "' + $('#newCustomer-address2').val() + '","Town": "' + $('#newCustomer-city').val() + '","Postcode": "' + $('#newCustomer-postcode').val() + '"}',
                    success: function () {
                        window.location.href = '#!/login';
                    }
                });
            } else {
                alert("You must give permission");
            }
            
        }
    });
});
